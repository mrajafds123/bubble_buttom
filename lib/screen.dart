import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        alignment: Alignment.center,
        color: Colors.red[100],
        child: Text('Home Screen'),
      ),
    );
  }
}

class RiwayatScreen extends StatelessWidget {
  const RiwayatScreen({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        alignment: Alignment.center,
        color: Colors.deepPurple[100],
        child: Text('Riwayat Screen'),
      ),
    );
  }
}

class FolderScreen extends StatelessWidget {
  const FolderScreen({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        alignment: Alignment.center,
        color: Colors.orange,
        child: Text('Folders Screen'),
      ),
    );
  }
}

class GalleryScreen extends StatelessWidget {
  const GalleryScreen({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        alignment: Alignment.center,
        color: Colors.green[200],
        child: Text('Gallery Screen'),
      ),
    );
  }
}
